module Msg exposing (..)

import Browser
import Http
import Url

type Msg = LinkClicked Browser.UrlRequest
    | RegistrationAgreeToTermsChanged Bool
    | RegistrationConfirmPasswordChanged String
    | RegistrationPasswordChanged String
    | RegistrationUsernameChanged String
    | RegistrationGotUsernameAvailability (Result Http.Error Bool)
    | UrlChanged Url.Url

