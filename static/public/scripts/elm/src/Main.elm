module Main exposing (main)

import Api
import Browser
import Browser.Navigation
import Html exposing (..)
import Html.Attributes exposing (..)
import Msg exposing (..)
import Models.Model exposing (Model)
import Models.RegistrationModel exposing (UsernameAvailability(..))
import Url
import Views.MainView

-- MAIN

main : Program () Models.Model.Model Msg
main =
  Browser.application
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    , onUrlChange = UrlChanged
    , onUrlRequest = LinkClicked
    }

-- MODEL

init : () -> Url.Url -> Browser.Navigation.Key -> ( Model, Cmd Msg )
init flags url key =
  (Models.Model.init key url, Cmd.none )

-- UPDATE

update : Msg -> Model -> (Model, Cmd Msg )
update msg model =
    case msg of
        LinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Browser.Navigation.pushUrl
                          model.key
                          (Url.toString url)
                    )
                Browser.External href ->
                    ( model, Browser.Navigation.load href )
        RegistrationAgreeToTermsChanged agree ->
            model.registration
                |> \registration -> { registration | agreeToTerms = agree }
                |> \r -> ({ model | registration = r }, Cmd.none)
        RegistrationGotUsernameAvailability result ->
            case result of 
                Ok is ->
                    model.registration
                        |> \r -> Models.RegistrationModel.updateAvailability
                                 model.registration is
                        |> \r1 -> { model | registration = r1 }
                        |> \m -> ( m, Cmd.none )
                Err error -> ( model, Cmd.none )
        RegistrationConfirmPasswordChanged cpw ->
            model.registration
                |> \registration -> { registration | confirmPassword = cpw }
                |> \r -> ({ model | registration = r }, Cmd.none)
        RegistrationPasswordChanged password ->
            model.registration
                |> \registration -> { registration | password = password }
                |> \r -> ({ model | registration = r }, Cmd.none)
        RegistrationUsernameChanged username->
            model.registration
                |> \registration -> { registration | username = username }
                |> \r -> { model | registration = r }
                |> \m -> ( m, Api.usernameAvailable username )
        UrlChanged url -> (Models.Model.updateUrl model url, Cmd.none)

-- SUBSCRIPTIONS

subscriptions : Model -> Sub Msg
subscriptions _ =
  Sub.none

-- VIEW

view : Model -> Browser.Document Msg
view model =
  { title = "Hunter / Gatherer"
  , body = [ Views.MainView.root model ]
  }
