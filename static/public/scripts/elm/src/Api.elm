module Api exposing (usernameAvailable)

import Http
import Json.Decode
import Json.Encode
import Msg

usernameAvailable : String -> Cmd Msg.Msg
usernameAvailable username =
    Http.post
        { url = "/api/username-available"
        , body = Http.jsonBody
                 ( Json.Encode.object
                      [ ("username", Json.Encode.string username ) ]
                 )
        , expect = Http.expectJson
                   Msg.RegistrationGotUsernameAvailability 
                   Json.Decode.bool
        }
