module Models.Model exposing (Model, init, updateUrl)

import Browser.Navigation
import Models.RegistrationModel
import Route
import Url

type alias Model =
  { key : Browser.Navigation.Key
  , registration : Models.RegistrationModel.RegistrationModel
  , route : Route.Route
  , url : Url.Url
  , username : Maybe String
  }

init : Browser.Navigation.Key -> Url.Url -> Model
init key url =
    { key = key
    , registration = Models.RegistrationModel.init
    , route = Route.parseUrl url
    , url = url
    , username = Nothing
    }

updateUrl : Model -> Url.Url -> Model
updateUrl model url = { model | route = Route.parseUrl url , url = url }

