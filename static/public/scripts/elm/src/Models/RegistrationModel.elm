module Models.RegistrationModel
    exposing (RegistrationModel, init, updateAvailability,
                  UsernameAvailability(..), validationRules
             )

import Models.ValidationRule
import Regex
        
type UsernameAvailability = Available | Loading | Unavailable

type alias RegistrationModel =
    { agreeToTerms : Bool
    , confirmPassword : String
    , password : String
    , username : String
    , usernameAvailable : UsernameAvailability
    }

init : RegistrationModel
init =
    { agreeToTerms = False
    , confirmPassword = ""
    , password = ""
    , username = ""
    , usernameAvailable = Unavailable
    }

updateAvailability : RegistrationModel -> Bool -> RegistrationModel
updateAvailability model available =
    if available
    then { model | usernameAvailable = Available }
    else { model | usernameAvailable = Unavailable }

validationRules : List (Models.ValidationRule.ValidationRule RegistrationModel)
validationRules =
    [ { alwaysDisplay = True
      , displayText = "A username is required."
      , fn = \model -> (String.length model.username) > 0
      }
    , { alwaysDisplay = True
      , displayText = "Usernames may contain alphanumeric characters, hyphens,"
                      ++ " and underscores."
      , fn = \model -> "[^\\w-]"
      |> Regex.fromString
      |> Maybe.withDefault Regex.never
      |> \regex -> not (Regex.contains regex model.username)
      }
    , { alwaysDisplay = True
      , displayText = "A password must be at least 16 characters."
      , fn = \model -> (String.length model.password) >= 16
      }
    , { alwaysDisplay = True
      , displayText = "The password confirmation should match."
      , fn = \model -> model.password == model.confirmPassword
      }
    , { alwaysDisplay = True
      , displayText = "Please agree to the terms of service."
      , fn = \model -> model.agreeToTerms
      }
    , { alwaysDisplay = False
      , displayText = "Usernames may be no more than 128 characters."
      , fn = \model -> (String.length model.username) <= 128
      }
    , { alwaysDisplay = False
      , displayText = "Passwords may be no more than 128 characters."
      , fn = \model -> (String.length model.password) <= 128
      }
    ]

