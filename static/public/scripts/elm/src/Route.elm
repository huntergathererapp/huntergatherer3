module Route exposing (Route(..), parseUrl)

import Url
import Url.Parser exposing ((</>), (<?>))
import Url.Parser.Query

type Route = Home
    | Register
    | NotFound

parseUrl : Url.Url -> Route
parseUrl url =
    case Url.Parser.parse matchRoute url of
        Just route -> route
        Nothing -> NotFound

-------------------------------------------------------------------------------

matchRoute : Url.Parser.Parser (Route -> a) a
matchRoute =
    Url.Parser.oneOf
        [ Url.Parser.map Home Url.Parser.top 
        , Url.Parser.map Register (Url.Parser.s "register")
        ]
