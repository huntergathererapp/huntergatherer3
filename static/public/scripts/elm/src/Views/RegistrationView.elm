module Views.RegistrationView exposing (root)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Models.RegistrationModel
import Msg
import Views.ValidationRuleView

root : Models.RegistrationModel.RegistrationModel -> Html Msg.Msg
root model =
    main_ [ class "leaf" ]
        [ h2 [ class "h2" ] [ text "Register" ]
        , label [ class "label-text" ]
            [ span [ class "label-span" ] [ text "Username" ]
            , input [ class "input-text"
                    , name "username"
                    , onInput Msg.RegistrationUsernameChanged
                    , required True
                    , type_ "text"
                    , value model.username
                    ] []
            ]
        , available model
        , label [ class "label-text" ]
            [ span [ class "label-span" ] [ text "Password" ]
            , input [ class "input-text"
                    , name "password"
                    , onInput Msg.RegistrationPasswordChanged
                    , required True
                    , type_ "password"
                    , value model.password
                    ] []
            ]
        , label [ class "label-text" ]
            [ span [ class "label-span" ] [ text "Confirm Password" ]
            , input [ class "input-text"
                    , name "confirm-password"
                    , onInput Msg.RegistrationConfirmPasswordChanged
                    , required True
                    , type_ "password"
                    , value model.confirmPassword
                    ] []
            ]
        , label []
            [  input [ checked model.agreeToTerms
                  , name "agreeToTerms"
                  , type_ "checkbox"
                  ] []
            , text " I agree to not be a jerk on this website."
            ]
        , Views.ValidationRuleView.root
            Models.RegistrationModel.validationRules model
        ]
-----------------------------------------------------------------------------
available : Models.RegistrationModel.RegistrationModel -> Html Msg.Msg
available model =
    case model.usernameAvailable of
        Models.RegistrationModel.Available ->
            div[] [ text "Username available" ]
        Models.RegistrationModel.Loading ->
            div [] [ text "Loading..." ]
        Models.RegistrationModel.Unavailable ->
            div [] [ text "Username unavailable" ]
