module Views.MainView exposing (root)

import Html exposing (..)
import Html.Attributes exposing (..)
import Models.Model
import Msg
import Route exposing (Route(..))
import Views.RegistrationView

root : Models.Model.Model -> Html Msg.Msg
root model = div [ class "container" ]
             [ header [ class "header" ]
                   [ h1 [ class "h1" ] [ text "Hunter / Gatherer" ]
                   , menu model.username
                   ]
             , mainBody model
             ]

------------------------------------------------------------------------------
loggedInMenu : String -> Html Msg.Msg
loggedInMenu username =
    nav [ ]
        [ a [ class "nav-a", href "/maintain" ] [ text "maintain" ]
        , a [ class "nav-a", href "/signout" ] [ text "sign out" ]
        ]

loggedOutMenu : Html Msg.Msg
loggedOutMenu =
    nav [ ]
        [ a [ class "nav-a", href "/register" ] [ text "register" ]
        , a [ class "nav-a", href "/signin" ] [ text "sign in" ]
        ]

mainBody : Models.Model.Model -> Html Msg.Msg
mainBody model =
    case model.route of
        Home -> main_ [ ] [ h2 [ class "h2" ] [ text "Home" ]]
        Register -> Views.RegistrationView.root model.registration
        NotFound -> main_ [ ] [ h2 [ class "h2" ] [ text "NotFound" ] ]

menu : Maybe String -> Html Msg.Msg
menu maybeUsername =
    case maybeUsername of
        Just username -> loggedInMenu username
        Nothing -> loggedOutMenu
