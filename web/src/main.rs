mod hg;
use crate::hg::environment;
use crate::hg::web::controller::index_controller;
use crate::hg::web::controller::user_controller;
use crate::hg::web::session;
use actix_web::{App, HttpServer};
use actix_web::web::{Data, get, post};
use deadpool_postgres::Config;
use postgres::{ Client, NoTls };
use std::env;
use std::sync::Mutex;
//use tokio_postgres::NoTls;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let port = environment::port(env::var("PORT"));
    HttpServer::new(|| {
        App::new()
            .app_data(Data::new(
                environment::config(std::env::vars())
                    .create_pool(None, tokio_postgres::NoTls)
                    .expect("Error initializing connection pool")
            ))
            .route("/", get().to(index_controller::index))
            .route("/username-available", post()
                   .to(user_controller::username_available2))
            .wrap(session::middleware())
    })
        .bind(("0.0.0.0", port))? 
        .run()
        .await
}
