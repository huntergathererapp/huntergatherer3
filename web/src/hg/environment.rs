use deadpool_postgres::{Config, ManagerConfig, RecyclingMethod};
use std::env::Vars;

pub fn config(vars: Vars) -> Config
{
    let mut result: Config = Config::new();
    for (key, value) in vars {
        match key.as_str() {
            "DB_NAME" => result.dbname = Some(value),
            "DB_HOST" => result.host = Some(value),
            "DB_PASSWORD" => result.password = Some(value),
            "DB_PORT" => result.port = value.parse::<u16>().ok(),
            "DB_USER" => result.user = Some(value),
            // Catch-all arm to discard irrelevant variables.
            _ => {}
        }
    }
    result.manager =
       Some(ManagerConfig { recycling_method: RecyclingMethod::Fast });
    result
}

/** parses the environment variable into a port number or fails over to 8080.*/
pub fn port(var: Result<String, std::env::VarError>) -> u16 {
    match var {
	Ok(port_string) => {
	    println!("Got port {port_string}.");
	    match port_string.parse() {
		Ok(port) => port,
		Err(_e) => {
		    print!("Failure to parse port {port_string}");
		    print!("Defaulting to port 8080");
		    8080
		}
	    }
	},
	Err(_e) => {
	    println!("No port environment variable. Defaulting to port 8080.");
	    8080
	}
    }
}

#[cfg(test)]
mod tests {
    use crate::environment::port;
    #[test]
    fn port_happy_path() {
	assert_eq!(port(Ok("3000".to_string())), 3000);
    }
}
