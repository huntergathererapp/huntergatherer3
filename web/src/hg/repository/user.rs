use postgres::Client;
use postgres::error::Error;

pub fn username_available(
    connection_string: String, username: String
) -> Result<bool, postgres::error::Error>
{
    let client_result =
	postgres::Client::connect(&connection_string, postgres::NoTls);
    match client_result {
	Ok(mut client) => {
	    let query = "select auth.username_available($1)";
	    match client.query_one(query, &[&username]) {
		Ok(row) => row.try_get("token"),
		Err(e) => Err(e)
	    }
	},
	Err(e) => Err(e)
	    
    }
}

pub fn username_available1(
    mut client: Client, username: String
) -> Result<bool, Error>
{
    let result_result: Result<Result<bool, Error>, Error> =
        client.query_one("select auth.username_available($1)", &[&username])
        .map(|row| row.try_get("token"));
    match result_result {
        Ok(result) => result,
        Err(e) => Err(e)
    }
}
