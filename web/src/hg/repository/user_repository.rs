use deadpool_postgres::Client;

pub async fn username_available(
    client: &Client, username: String
) -> bool
{
    let query = "select auth.username_available($1)";
    let rows = client.query(query, &[&username])
        .await
        .unwrap();
    rows[0].get(0)
}
