use actix_web::{ HttpResponse, Responder };
use actix_web::web::{Data, Json};
use crate::hg::model::user_model::UsernameModel;
use crate::hg::repository;
use deadpool_postgres::{ Client, Pool, PoolError };
use postgres::error::Error;
use std::env;
use std::sync::Mutex;

pub async fn username_available(model: Json<UsernameModel>) -> impl Responder
{
    match env::var("CONNECTION_STRING") {
        Ok(connection_string) => {
            match repository::user
                ::username_available(connection_string, model.username.clone())
            {
                Ok(available) => HttpResponse::Ok().json(available),
                Err(_) => HttpResponse::InternalServerError()
                    .body("Database error.")
            }
        },
        Err(_) => HttpResponse::InternalServerError()
            .body("Connection string not set.")
    }
    
}

pub async fn username_available2(
    model: Json<UsernameModel>, pool: Data<Pool>
) -> impl Responder
{
    match pool.get().await
    {
        Ok(client) =>     
        {
            let available = repository::user_repository
                ::username_available(&client, model.username.clone())
                .await;
            HttpResponse::Ok().json(available)
        },
        Err(pool_error) =>
        {
            match pool_error
            {
                PoolError::Timeout(_) => HttpResponse::InternalServerError()
                    .body("Database error: timeout."),
                PoolError::Backend(e) => HttpResponse::InternalServerError()
                    .body(format!("Database error: backend: {:?}", e)),
                PoolError::Closed => HttpResponse::InternalServerError()
                    .body("Database error: closed."),
                PoolError::NoRuntimeSpecified =>
                    HttpResponse::InternalServerError()
                    .body("Database error: no runtime specified."),
                _ => HttpResponse::InternalServerError()
                    .body("Database error: something about hooks.")
            }
        }
    }
}
