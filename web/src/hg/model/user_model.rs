use serde;

#[derive(serde::Deserialize)]
pub struct UsernameModel {
    pub username: String
}
