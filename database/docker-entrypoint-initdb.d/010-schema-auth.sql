create schema if not exists auth;

create extension if not exists pgcrypto with schema auth;

---------------------------------users-----------------------------------------

create table if not exists
auth.users (
  password text not null check (length(password) < 512),
  user_id uuid primary key default gen_random_uuid(),
  username text unique not null
);

create or replace function
auth.hash_password() returns trigger as $$
begin
  if tg_op = 'INSERT' or new.password <> old.password then
    new.password = auth.crypt(new.password, auth.gen_salt('bf'));
  end if;
  return new;
end
$$ language plpgsql;

drop trigger if exists hash_password on auth.users;
create trigger hash_password
  before insert or update on auth.users
  for each row
  execute procedure auth.hash_password();

-----------------------------------auth.tokens---------------------------------

create table if not exists
auth.tokens(
  user_id uuid not null,
  token text primary key,
  created timestamp default current_timestamp,
  valid boolean not null default true,
  constraint fk_tokens_to_users foreign key(user_id)
             references auth.users(user_id)
);

create or replace function
auth.hash_token() returns trigger as $$
begin
  if tg_op = 'INSERT'
  then new.token = encode(auth.digest(new.token, 'sha256'), 'hex');
  end if;
  return new;
end
$$ language plpgsql;


drop trigger if exists hash_token on auth.tokens;
create trigger hash_password
  before insert or update on auth.tokens
  for each row
  execute procedure auth.hash_token();

---------------------------procedures and functions----------------------------

create or replace function
auth.check_password(username text, password text) returns boolean as $$
begin
  return exists (
         select 1 from auth.users
         where auth.users.username = check_password.username
         and auth.users.password
             = auth.crypt(check_password.password, auth.users.password)
  );
end
$$ LANGUAGE plpgsql;

create or replace function
auth.check_token(username text, token text) returns boolean as $$
begin
  return exists(
    select 1 from auth.tokens
    join auth.users on auth.tokens.user_id = auth.users.user_id
    where auth.tokens.valid
    and auth.tokens.token = auth.crypt(check_token.token)
  );
end;
$$ LANGUAGE plpgsql;

create or replace function
auth.login(username text, password text) returns text as $$
declare
  cryptext text;
  token text;
begin
  if auth.check_password(username, password) then
    begin
      token := encode(auth.gen_random_bytes(16), 'hex');
      insert into auth.tokens(user_id, token)
      select user_id, token
      from auth.users
      where auth.users.username = login.username;
      return token;
    end;
  else
    begin
      raise invalid_password using message = 'invalid username or password.';
    end;
  end if;
end;
$$ LANGUAGE plpgsql;

create or replace procedure
auth.logout(username text, token text) as $$
begin
  if auth.check_token(username, token) then
    begin
      update auth.tokens set valid = false
        where auth.tokens.username = logout.username
	and auth.tokens.token = logout.username;
    end;
  else
    begin
      raise invalid_password using message = 'invalid token for user.';
    end;
  end if; 
end;
$$ LANGUAGE plpgsql;

create or replace procedure
auth.logout_all(username text, token text) as $$
begin
  if auth.check_token(username, token) then
    begin
      update auth.tokens set valid = false
      from auth.users
      where auth.users.username = logout_all.username
      and auth.users.user_id = auth.tokens.user_id;
    end;
  else
    begin
      raise invalid_password using message = 'invalid token for user.';
    end;
  end if; 
end;
$$ LANGUAGE plpgsql;


create or replace function
auth.register(username text, password text) returns text as $$
begin
  if auth.username_available(username) then
    begin
      insert into auth.users (username, password) values (username, password);
      return auth.login(username, password);
    end;
  else
    begin
      raise exception 'Username already registered.';
    end;
  end if;
end;
$$ LANGUAGE plpgsql;

create or replace procedure
auth.reset_password(username text, old_password text, new_password text)
as $$
begin
  if auth.check_password(username, old_password) then
    begin
      update auth.users set password = new_password
      where auth.users.username = reset_password.username;

      update auth.tokens set valid = false
      from auth.users
      where auth.users.username = reset_password.username
      and auth.users.user_id = auth.tokens.user_id;
      
    end;
  else
    begin
      raise invalid_password using message = 'incorrect old password.';
    end;
  end if;
end;
$$ language plpgsql;

create or replace function
auth.username_available(username text) returns boolean as $$
begin
  return not exists(
    select 1 from auth.users
    where auth.users.username = username_available.username
  );
end;
$$ LANGUAGE plpgsql;

